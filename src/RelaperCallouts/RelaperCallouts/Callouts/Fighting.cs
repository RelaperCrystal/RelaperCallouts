using System;
using LSPD_First_Response.Mod.Callouts;
using RelaperCallouts.Callouts.Framework;
using RelaperCallouts.Util;
using Rage;
using LSPD_First_Response.Mod.API;
using RelaperCommons;
using RelaperCommons.Entities;

namespace RelaperCallouts.Callouts
{
    [CalloutInfo("RC Fighting", CalloutProbability.Medium)]
    [ExperimentalCallout(DebugModeOnly = true)]
    public class Fighting : CalloutBase
    {
        private Ped firstPed;

        private Ped secondPed;

        private Blip areaBlip;

        private bool approach;

        private LHandle pursuit;

        protected override string Name => "Fighting in progress";

        protected override string ScannerCrimeName => "ASSAULT_ON_A_CIVILIAN";

        protected override bool HasNormalCrimeAudio => true;

        protected override bool HasReportCrimeAudio => false;

        public override bool OnBeforeCalloutDisplayed()
        {
            ResponseType = CalloutResponseType.Code2;
            ReportedByUnits = false;
            if (SpawnUtil.TryGenerateSpawnPointOnPedWalk(350f, 650f, false, out Vector3 spawn))
            {
                SpawnPoint = spawn;
            }
            else
            {
                terminated = false;
                Game.LogTrivial("RC: Unable to find a good spawn for call");
                return false;
            }

            this.AddMaximumDistanceCheck(750f, SpawnPoint);
            this.AddMinimumDistanceCheck(300f, SpawnPoint);

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            firstPed = new Ped(SpawnPoint.Around(3.0f))
            {
                IsPersistent = true,
                BlockPermanentEvents = true
            };
            secondPed = new Ped(SpawnPoint.Around(3.0f))
            {
                IsPersistent = true,
                BlockPermanentEvents = true
            };

            areaBlip = new Blip(World.GetNextPositionOnStreet(SpawnPoint.Around(50f)));
            areaBlip.SetColor(BlipColor.LighterYellow);
            areaBlip.IsRouteEnabled = true;

            Game.DisplaySubtitle("Go to ~y~" + World.GetStreetName(World.GetStreetHash(CalloutPosition)) + ".");
            return base.OnCalloutAccepted();
        }

        public override void Process()
        {
            base.Process();

            if (!firstPed.Exists() || !secondPed.Exists()) EndSuccess();

            // When player is close, the suspect will now aim the victim.
            // This is observed and learned from Fighting call-out in United Call-outs.
            if (!approach && Game.LocalPlayer.Character.DistanceTo(firstPed) < 50f && firstPed.IsOnScreen)
            {
                approach = true;
                areaBlip.Delete();

                firstPed.Tasks.FightAgainst(secondPed);
                secondPed.Tasks.FightAgainst(firstPed);

                pursuit = Functions.CreatePursuit();
                Functions.SetPursuitInvestigativeMode(pursuit, true);
                Functions.AddPedToPursuit(pursuit, firstPed);
                Functions.AddPedToPursuit(pursuit, secondPed);
                Functions.SetPursuitIsActiveForPlayer(pursuit, true);
                Functions.SetPursuitCopsCanJoin(pursuit, true);

                Game.DisplaySubtitle("Chase down ~r~suspects.");
            }

            if (!firstPed || firstPed.IsDead || Functions.IsPedArrested(firstPed) || !secondPed || secondPed.IsDead || Functions.IsPedArrested(secondPed))
            {
                EndSuccess();
            }
        }

        public override void End()
        {
            base.End();

            if (firstPed && !Functions.IsPedArrested(firstPed)) firstPed.Dismiss();
            if (secondPed && !Functions.IsPedArrested(secondPed)) secondPed.Dismiss();

            if (areaBlip) areaBlip.Delete();

            if (pursuit != null && Functions.IsPursuitStillRunning(pursuit))
            {
                Functions.ForceEndPursuit(pursuit);
            }
        }
    }
}
