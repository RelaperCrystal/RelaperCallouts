﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using System;
using System.Collections.Generic;
using LSPD_First_Response;
using LSPD_First_Response.Mod.API;
using LSPD_First_Response.Mod.Callouts;
using Rage;
using RelaperCallouts.Callouts.Framework;
using RelaperCallouts.Extern;
using RelaperCallouts.Util;
using RelaperCommons;
using RelaperCommons.Entities;

namespace RelaperCallouts.Callouts
{
    [CalloutInfo("Shootout", CalloutProbability.Medium)]
    public class Shootout : CalloutBase
    {
        private readonly List<Ped> Peds = new List<Ped>();
        private readonly List<Ped> DealtPeds = new List<Ped>();
        private readonly Dictionary<Ped, Blip> PedBlips = new Dictionary<Ped, Blip>();
        private static readonly RelationshipGroup ShooterGroup = new RelationshipGroup("RCSHOTGRP");
        private bool discovered;

        protected override string Name => "Shots fired";

        protected override string ScannerCrimeName => "SHOOTING";

        protected override bool HasNormalCrimeAudio => true;
        protected override bool HasReportCrimeAudio => false;

        // We need this to be called for Initialize
        // to start shooter group...
        internal static void InitShooterGroup()
        {
            ShooterGroup.SetRelationshipWith(ShooterGroup, Relationship.Hate);
            ShooterGroup.SetRelationshipWith(RelationshipGroup.Cop, Relationship.Hate);
            ShooterGroup.SetRelationshipWith(RelationshipGroup.Player, Relationship.Hate);
        }

        public override bool OnBeforeCalloutDisplayed()
        {
            ResponseType = CalloutResponseType.Code3;
            ReportedByUnits = MathHelper.GetRandomInteger(11) != 5;
            SpawnPoint = SpawnUtil.GenerateSpawnPointAroundPlayer(200f, 500f);
            ReportedByUnits = false;

            this.AddMinimumDistanceCheck(150f, SpawnPoint);
            this.AddMaximumDistanceCheck(550f, SpawnPoint);

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            var spawnAmount = MathHelper.GetRandomInteger(4, 15);
            for (int i = 0; i < spawnAmount; i++)
            {
                GameFiber.Yield();
                Ped ped;

                if (ExternManager.ResponseReplaceInstalled)
                {
                    ped = ResponseReplaceFunctions.CreatePed(SpawnPoint.Around(3.3f, 12.2f));
                    ped.IsPersistent = true;
                }
                else
                {
                    ped = new Ped(SpawnPoint.Around(3.3f, 12.2f))
                    {
                        IsPersistent = true
                    };
                }

                ped.Inventory.GiveNewWeapon(WeaponHash.Pistol, 200, true);
                ped.RelationshipGroup = ShooterGroup;
                var blip = ped.AttachBlip();
                blip.Scale = 0.50f;
                blip.Sprite = BlipSprite.Enemy;
                blip.SetColor(BlipColor.Red);
                blip.IsFriendly = false;
                PedBlips.Add(ped, blip);

                Peds.Add(ped);
            }

            Blip = new Blip(World.GetNextPositionOnStreet(SpawnPoint.Around(10f)), 65f);
            Blip.SetColor(BlipColor.Yellow);
            Blip.SetRouteColor(BlipColor.Yellow);
            Blip.IsRouteEnabled = true;

            Game.DisplaySubtitle("Get to the ~y~crime scene~w~.");

            return base.OnCalloutAccepted();
        }

        public override void Process()
        {
            if (!discovered && Game.LocalPlayer.Character.Position.DistanceTo2D(CalloutPosition) < 25f)
            {
                discovered = true;
                Game.DisplaySubtitle("Arrest or take out the ~r~suspect~w~s.");
                Blip.IsFriendly = false;
                Blip.Flash(500, -1);
                Blip.Alpha = 0.45f;
                Blip.IsRouteEnabled = false;
                if (ExternManager.UltimateBackupInstalled)
                {
                    UltimateBackupFunctions.RequestLocalUnitCodeThree();
                }
                else
                {
                    Functions.RequestBackup(SpawnPoint, EBackupResponseType.Code3, EBackupUnitType.LocalUnit);
                }
            }

            for (int i = 0; i < Peds.Count; i++)
            {
                GameFiber.Yield();
                var ped = Peds[i];
                if (!ped || !ped.IsAlive || Functions.IsPedArrested(ped))
                {
                    Peds.RemoveAt(i);
                    if (ped)
                    {
                        PedBlips[ped].Delete();

                        DealtPeds.Add(ped);
                    }
                }
            }

            if (Peds.Count == 0)
            {
                EndSuccess();
            }

            base.Process();
        }

        public override void End()
        {
            base.End();

            foreach (var ped in Peds)
            {
                if (ped && !Functions.IsPedArrested(ped))
                {
                    if (ped.IsAlive && ExternManager.ResponseReplaceInstalled)
                    {
                        try
                        {
                            ResponseReplaceFunctions.RegisterPed(ped);
                        }
                        catch (Exception ex)
                        {
                            Game.LogTrivial(ex.ToString());
                        }
                    }
                    ped.Dismiss();
                }
            }

            foreach (var ped in DealtPeds)
            {
                if (ped && !Functions.IsPedArrested(ped)) ped.Dismiss();
            }

            foreach (var blip in PedBlips)
            {
                if (blip.Value) blip.Value.Delete();
            }
        }
    }
}
