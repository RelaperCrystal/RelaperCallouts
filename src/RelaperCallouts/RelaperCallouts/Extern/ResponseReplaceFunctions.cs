﻿using System;
using Rage;
using ResponseReplace.Api;

namespace RelaperCallouts.Extern
{
    internal static class ResponseReplaceFunctions
    {
        /// <inheritdoc cref="Functions.CreatePed(Vector3, float)"/>
        public static Ped CreatePed(Vector3 position, float heading = 0f) => Functions.CreatePed(position, heading);

        public static void RegisterPed(Ped ped) => Functions.RegisterCreatedPed(ped);

        public static void DismissPed(Ped ped)
        {
            if (ped.IsAlive && ExternManager.ResponseReplaceInstalled)
            {
                try
                {
                    RegisterPed(ped);
                }
#pragma warning disable CA1031 // Do not catch general exception types
                catch (Exception ex)
                {
                    Game.LogTrivial(ex.ToString());
                }
#pragma warning restore CA1031 // Do not catch general exception types
            }
            if (!LSPD_First_Response.Mod.API.Functions.IsPedArrested(ped))
            {
                ped.Dismiss();
            }
        }
    }
}
