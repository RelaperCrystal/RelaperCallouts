﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using LSPD_First_Response.Mod.API;
using Rage;

namespace RelaperCallouts.Extern
{
    internal static class ExternManager
    {
        internal static bool StopThePedInstalled { get; private set; }
        internal static bool UltimateBackupInstalled { get; private set; }
        internal static bool ResponseReplaceInstalled { get; private set; }

        internal static void Init()
        {
            GameFiber.Sleep(500);
            Game.LogTrivial("Rel.C: Initializing external plugins");
            foreach (var plugin in Functions.GetAllUserPlugins())
            {
                // Prevents multiple plug-ins loading at same time
                // Wasting resources
                GameFiber.Yield();
                switch (plugin.GetName().Name)
                {
                    case "StopThePed":
                        Game.LogTrivial("Rel.C: Found Stop The Ped");
                        StopThePedInstalled = true;
                        break;
                    case "UltimateBackup":
                        Game.LogTrivial("Rel.C: Found Ultimate Backup");
                        UltimateBackupInstalled = true;
                        break;
                    case "ResponseReplace":
                        Game.LogTrivial("Rel.C: Found Response Replace");
                        ResponseReplaceInstalled = true;
                        break;
                }
            }
        }
    }
}
