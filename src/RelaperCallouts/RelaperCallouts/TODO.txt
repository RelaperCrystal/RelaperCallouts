﻿=========================================
LEGENDS
=========================================
"-" means a bug, crash or malfunction
"*" means a TODO except for new call-out
"+" means a new call-out requirement
"#" means a test

BUGS
------------------------------------------------------------------
- Foot pursuit ends before pursuit ends, due to invalid officer

TODO
------------------------------------------------------------------
// INVESTIGATING // - Vehicle Window invalid in Stolen Emergency Vehicle
// INVESTIGATING // - Blip don't disappear in Stolen Emergency Vehicle
# TESTING: Stolen Emergency Vehicle
+ NEW CALLOUT: Gun Fight
+ NEW CALLOUT: Assault
+ NEW CALLOUT: Firearm attack on officer
+ NEW CALLOUT: Troublemaker at Public
* Ambient Event system