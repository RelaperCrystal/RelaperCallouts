﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using LSPD_First_Response.Mod.API;
using Rage;
using RelaperCallouts.Callouts.Framework;

namespace RelaperCallouts.Util
{
    internal static class ScannerMessages
    {
        internal static string GetResponseCodeStr(CalloutResponseType code)
        {
            string codeSound = "RC_";

            switch (code)
            {
                default:
                    codeSound += "CODE2";
                    break;
                case CalloutResponseType.Code3:
                    codeSound += "CODE3";
                    break;
                case CalloutResponseType.Code99:
                    codeSound += "CODE99";
                    break;
            }

            return codeSound;
        }

        internal static void EndCall(string name)
        {
            Functions.PlayScannerAudio("RC_STANDDOWN", true);
            DisplayDispatchText(name, "We're ~g~Code 4~w~. All units stand down, return to ~b~patrol~w~.");
        }

        internal static void DisplayDispatchText(string caption, string text)
        {
            Game.DisplayNotification("web_lossantospolicedept", "web_lossantospolicedept", "Dispatch", caption, text);
        }

        internal static void DisplayPlayerResponding()
        {
            Game.DisplayNotification($"~b~Officer {Functions.GetPersonaForPed(Game.LocalPlayer.Character).FullName}~w~: Responding.");
        }
    }
}
